mod sample;

use sample::Sample;

#[derive(Debug)]
pub struct Song {
    name: String,
    num_samples: u32,
    samples: Vec<Sample>,
    num_patterns: u8,
    end_position: u8,
    patterns: Vec<u8>,
}

impl Song {
    pub fn new(data: Vec<u8>) -> Song {
        let song_name = String::from_utf8_lossy(&data[0..20]);
        let num_samples = get_num_samples(&data);
        let mut offset: usize = 20;
        let samples: Vec<Sample> = load_samples(&data, num_samples, &mut offset);
        
        let num_patterns: u8 = data[offset];
        let end_position: u8 = data[offset + 1];     
        offset += 2;
        let patterns: Vec<u8> = read_pattern(&data, &mut offset);

        Song {
            name: song_name.to_ascii_lowercase(),
            num_samples: num_samples,
            samples: samples,
            num_patterns: num_patterns,
            end_position: end_position,
            patterns: patterns,
        }
    }
}

fn get_num_samples(data: &Vec<u8>) -> u32 {
    let format_tag = String::from_utf8_lossy(&data[1080..1084]);
    match format_tag.as_ref() {
        "M.K." => 31,
        _ => 15
    }
}

fn load_samples(data: &Vec<u8>, num_samples: u32, offset: &mut usize) -> Vec<Sample> {
    let mut samples: Vec<Sample> = Vec::new();

    for _sample_num in 0..num_samples {
        samples.push(Sample::new(&data[*offset..(*offset + 30) as usize]));
        *offset += 30;
    }
    samples
}

fn read_pattern(data: &Vec<u8>, offset: &mut usize) -> Vec<u8> {
    let pattern_table: Vec<u8> = data[*offset..(*offset + 128)].to_vec();
    *offset += 128;
    pattern_table
}
