use std::fs;
use crate::song::Song;

pub fn load_file(filename: &str) -> Song {
    let file: Vec<u8> = match fs::read(filename) {
        Err(why) => panic!("Filename couldn't be found {}: {}", filename, why),
        Ok(data) => data
    };
    
    let song = Song::new(file);
    println!("{:?}", song);

    song
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_load_file() {
        super::load_file("bubble_bobble.mod");
    }

    #[test]
    #[should_panic]
    fn test_missing_file() {
        super::load_file("missing file");
    }
}