mod loader;
mod song;

pub fn play_file(filename: &str) {
    loader::load_file(filename);
}